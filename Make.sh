#!/bin/bash

currentSource=$1
currentBinary=$2
installDir=$3

compileSet() {
    setname=$1
    currentBinary=$2
    currentSource=$3
    mkdir -p $setname
    cd $setname
    cp ${currentSource}/${setname}/*.sty .
    for file in `ls ${currentSource}/${setname}/*.tex`
    do
        piece=$(basename $file ".tex")
        xelatex ${currentSource}/${setname}/${piece}.tex > /dev/null
        pdf2svg ${piece}.pdf ${piece}.svg > /dev/null
    done
    for piece in `ls ${currentSource}/${setname}/*.svg`
    do
        cp ${piece} .
    done
    cd $currentBinary
}

cleanTexTmp() {
    for extension in aux log pdf
    do
        \rm -f *.$extension
    done
}

installSet() {
    installDir=$2
    set=$1
    mkdir -p ${installDir}/${set}
    for icon in `ls ${set}/*.svg`
    do
        cp $icon ${installDir}/${icon}
    done
}

for set in `ls ${currentSource}`
do
    if [[ -d ${currentSource}/${set} ]]
    then
        echo "Building icon set \`${set}'..."
        compileSet $set $currentBinary $currentSource
        if [[ -n $installDir ]]
        then
            echo "Installing icon set \`${set}\' in \`${installDir}'..."
            installSet $set $installDir
        fi
    fi
done
